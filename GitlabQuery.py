#!/usr/bin/env python

import requests

"""
#	
# GitLabQuery,py
#
# @author: Sachin Janardan Gamre <sachin.gamre@gmail.com>
#
"""
class GitlabQuery:

	# Class attributes
	__gitLabGroupsApi = "/api/v4/groups/"
	__defaultBranch = 'master'


	def __init__(self, gitlabDomain, privateToken):
		self.__privateToken = privateToken
		self.__gitlabDomain = gitlabDomain

	"""
	#
	# Fetch all groups for given token.
	# Returns API response.
	#
	# @access: private
	#
	"""	
	def __fetchAllGroups(self):
		url = self.__gitlabDomain + '/' + self.__gitLabGroupsApi
		response = requests.get(
				url,
				headers = { 'Private-Token': self.__privateToken },
				timeout = 10 
			)
		jsonResponse = response.json()
		return jsonResponse

	"""
	#
	# Fetch all projects in the given group
	# Return API response.
	#
	# @access: private
	#
	"""	
	def __fetchGroupProjects(self, groupName, groupId):
		url = self.__gitlabDomain + '/' + self.__gitLabGroupsApi + '/' + str(groupId) + '/projects'
		response = requests.get(
				url,
				headers = { 'Private-Token': self.__privateToken },
				timeout = 10 
			)
		jsonResponse = response.json()
		return jsonResponse

	"""
	#
	# Fetch last committed sha for given project for given branch name.
	# Prints the sha on standard output.
	#
	# @access: private
	#
	"""
	def __fetchLastCommitForProject(self, projectName, projectId, repoLink, branchName):
		url = repoLink + '/' + branchName
		response = requests.get(
				url,
				headers = { 'Private-Token': self.__privateToken },
				timeout = 10 
			)
		if response.status_code == 200:
			jsonResponse = response.json()
			print(projectName + " " + branchName + " " + jsonResponse['commit']['short_id'])
		else:
			if branchName != self.__defaultBranch:
				self.__fetchLastCommitForProject(projectName, projectId, repoLink, self.__defaultBranch)
			else:
				print("No branches in " + projectName)	

	"""
	#
	# Displays all group projects.
	# Prints the project names on standard output.
	# 
	# @access: Public
	# 
	"""			
	def displayGroupProjects(self, groupName):
		groupResponse = self.__fetchAllGroups()
		for group in groupResponse:
			breakLoop = False
			if groupName:
				breakLoop = True
				if group['name'].find(groupName) == -1:
					continue

			projectResponse = self.__fetchGroupProjects(groupName, group['id'])
			for project in projectResponse:
				print(project['name'])		

			if bool(breakLoop):
				break	
	
	"""
	#
	# Displays all groups, in which the user token is associated. 
	# Prints the group name on standard output.
	#
	# @access: Public
	#
	"""				
	def displayAllGroups(self):
		jsonResponse = self.__fetchAllGroups() 
		for group in jsonResponse:
			print(group['name'])


	"""
	#
	# Displays last commit sha for given branch in given project in given group name.
	# Prints the sha on standard output.
	#
	# @access: Public
	#
	"""		
	def displayLastCommitForProject(self, projectName, branchName, groupName):
		groupResponse = self.__fetchAllGroups()

		for group in groupResponse:
			breakLoop = False

			if groupName:
				breakLoop = True
				if group['name'].find(groupName) == -1:
					continue

			projectResponse = self.__fetchGroupProjects(groupName, group['id'])
			for project in projectResponse:
				breakLoop1 = False
				if projectName and projectName.upper() != 'ALL':
					breakLoop1 = True
					if projectName != project['name']:
						continue

				self.__fetchLastCommitForProject(project['name'], project['id'], project['_links']['repo_branches'], branchName)

				if bool(breakLoop1):
					break


			if bool(breakLoop):
				break		
