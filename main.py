#!/usr/bin/env python

import configparser
from GitLabQuery import GitLabQuery
		
config = configparser.ConfigParser(allow_no_value=True);
config.read('config.ini')

gitQuery = GitLabQuery(config['DEFAULT']['gitlab-domain'], config['DEFAULT']['private-token'])
gitQuery.displayLastCommitForProject('all', config['DEFAULT']['branch-name'], config['DEFAULT']['group-name'])
